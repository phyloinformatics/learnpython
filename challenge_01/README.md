# README

Today's class is about basic scripting using the Linux/Unix shell. You will create one or more Bash scripts to solve the specific challenges listed below.

## Input

The input data for this challenge is the compressed file `input.zip`.

## Mini-Challenge 1.1

Write a Bash script that unpacks the contents of `input.zip` and checks the total disk size of the uncompressed directory. The Bash script should also print this disk size into the standard output. The total size should be displayed in a human-readable format (e.g., using KB, MB, or GB).

## Mini-Challenge 1.2

Write a Bash script that organizes the `input/` data into subdirectories based on the file extensions.

## Mini-Challenge 1.3

Write a Bash script that counts each file in the subdirectories created in the second mini-challenge and shows the result in the standard output.
