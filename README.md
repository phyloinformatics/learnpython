# Learn Python with the Phyloinformatics Lab

## About

Hi! I am [Dr. Denis Jacob Machado](https://phyloinformatics.com/members/Denis_Jacob_Machado.html) and this project invites you to learn Python with us through a series of challenges. We designed these materials for people that already have some begginer-level Python skills and the goal is to help those individuals improving those skills, learning more advanced Python, and creating code that is clean and maintainable. I specifically prepared these materials in the Fall of 2024 as part of a graduate-level course in the [University of North Carolina at Charlotte's (UNC Charlotte)](https://www.charlotte.edu/) [Dep. of Bioinformatics and Genomics](https://cci.charlotte.edu/departments/department-of-bioinformatics-and-genomics/) (course's codes: BINF-6112 and BINF-8112 combined, course's name: "Programming II").

## Programming II

Programming II (BINF-6112 and BINF-8112 combined) is is a mandatory class for Bioinformatics grad students that is open to Health Informatics Students. Some students have advanced Python knowledge, but most only know the very basics and don't have any computer science background other than Programming I.

In Programming I, students learned basic Python concepts like if-else clauses, for and while loops, and using lists and dictionaries. No, in Programming II, students shall be introduced to more advanced coding, including object-oriented design, classes, Pandas, and the differences between Python and Java.

Attendance is not a factor for grading but the classes are in person. There are three exams per semester, a final project, and several lab activities that are considered for evaluation. Furthermore, Programming II has a “flip classroom” style and preparation for classes also impacts the final grade.

The exams combine multiple-choice questions and coding problems (I will propose paper-based tests only).

Normally, this class has a TA that is responsible for the lab time.

## Main References

Programming two is havily inpired by the following materials:

- [Foundations of Python Programming](https://runestone.academy/ns/books/published/fopp/index.html?mode=browsing)
- [Foundations of Python Programming: Functions First](https://runestone.academy/ns/books/published/foppff/frontmatter-1.html?mode=browsing)
- [How to Think Like a Computer Scientist: Interactive Edition](https://runestone.academy/ns/books/published/thinkcspy/index.html?mode=browsing)
- [How to Think like a Data Scientist](https://runestone.academy/ns/books/published/httlads/index.html?mode=browsing)
- "Python Crash Course: A Hands-On, Project-Based Introdiuction to Programming" (3rd edition), by Eric Matthews
- "Python for Data Analysis: FData Wrangling with Pandas, Numpy, and IPython" (2nd edition), by Wes McKinney
- "Fluent Python: Clear, Concise, and Effective Programming" (2nd edition), by Luciano Ramalho

## Basic Details

- **Instructor:** Dr. Denis Jacob Machado (he/him/his). My last name is Jacob Machado, and I do not have a middle name. You can call me Denis
- **Instructor's Office:** BINF 453 (UNC Charlotte's CIPHER center, on the 4th floor of the Bioinformatics Building)
- **Email:** dmachado@charlotte.edu
- **Office Hours:** schedule meetings using [calendly.com/machadodj](https://calendly.com/machadodj)
- **Lab Page:** [phyloinformatics.com](https://phyloinformatics.com/)
- **Course's Codes:** BINF 6112 (Master's) / BINF 8112 (Ph.D.)
- **Course's Name:** Programming II
- **Number of Credits:** 4
- **Locality:** In person classes and lab sessions will be at UNC Charlotte's Department of Bioinformatics and Genomics, room 217 (BINF 217)
- **Time:** Classes are on Tuesdays and Thursdays from 4:00 pm to 5:15 pm, with lab sessions on Thursdays from 5:30 pm to 6:45 pm.
- **Days of the week:** TR (Tuesdays and Thursdays)
- **Date range:** Fall (from August to December); please check UNC Charlotte's academic calendar
- **Lecture Schedule Type/ Instructional Method:** face-to-face instructional method (all classes and lab sessions are in-person)
- **Prerequisites and restrictions:** Must have concluded Programming I (BINF 6111/8111)

**For additional details, please see the syllabus on CANVAS.**

## This project

This page contains materials for different challenges and classroom activities organized in several directories and subdirectories. The directories are numbered in the order activities will be completed in the classroom. Each directpry contains a `README.md` file detailing its contents. This project will be contantly modified throughout the year of 2024 until the classes are over.
