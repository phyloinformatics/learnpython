# README

Today's class is about basic scripting using the Linux/Unix shell. You will create one or more Bash scripts to solve the challenges listed below.

## Input

The input data for this challenge is the compressed file `sequence.gb.zip`.

## Mini-Challenge 2.1

Write a Bash script that unpacks the contents of `sequence.gb.zip` and prints the number of genomes inside. How many genomes are inside this file?

## Mini-Challenge 2.2

Write a Bash script that gets me, in alphabetical order and without repetitions, all the accession numbers (with version number) inside `sequence.gb.zip`. What is the number of unique accession numbers?

## Mini-Challenge 2.3

Write a Bash script that creates one directory for each unique accession number inside `sequence.gb.zip`. In each of those directories, your script shall create a text file named according to the corresponding accession number with the `txt ` extension. In each `txt` file, store information corresponding to that genome. The information to be stored are the LOCUS, DEFINITION, ACCESSION, and VERSION lines of the corresponding genome.

## Mini-Challenge 2.4

Write a Bash script that can go on each subdirectory inside the work directory and remake the text (`.txt`) files inside, making so that their prefix is `mod_` and their suffix is `.md`. The substring `.txt` should be absent in the modified files. After you are done, create a single `.txt` file that concatenates the information of all `.md` files that have the number 7 in their names.
